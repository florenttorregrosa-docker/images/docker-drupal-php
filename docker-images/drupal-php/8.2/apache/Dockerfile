FROM php:8.2-apache

ENV DEBCONF_FRONTEND=non-interactive
ENV DEBIAN_FRONTEND=noninteractive
ENV WEBSERVER_GROUP=www-data
ENV WEBSERVER_USER=www-data

# Even if not used because of https://github.com/moby/moby/issues/34482.
ARG COMPOSER_VERSION=2
ARG DRUSH_LAUNCHER_VERSION=latest

# Get Composer from official image.
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# Add some Bash aliases.
RUN echo 'alias ll="ls -l"' >> $HOME/.bashrc \
    && echo 'alias lll="ls -al"' >> $HOME/.bashrc \
    # Add bins from Composer's global vendor to be in PATH.
    && echo 'PATH="$HOME/.composer/vendor/bin:$PATH"' >> $HOME/.bashrc

# Install Drush Launcher.
RUN curl -sSL "https://github.com/drush-ops/drush-launcher/releases/${DRUSH_LAUNCHER_VERSION}/download/drush.phar" -o /usr/local/bin/drush \
    && chmod +x /usr/local/bin/drush

RUN apt-get update \
    && apt-get install -y \
        apt-utils \
        cron \
        git \
        gnupg \
        imagemagick \
        jq \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libjpeg-turbo-progs \
        libjpeg62-turbo-dev \
        libonig-dev \
        libpcre3-dev \
        libpng-dev \
        libpq-dev \
        libwebp-dev \
        libxml2-dev \
        libzip-dev \
        mariadb-client \
        msmtp \
        pngquant \
        python3 \
        redis-tools \
        rsync \
        sudo \
        unzip \
        wget \
        zlib1g-dev \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
        --with-webp \
    # The readline extension is already installed in the base image.
    && docker-php-ext-install \
        bcmath \
        curl \
        exif \
        gd \
        intl \
        mbstring \
        mysqli \
        opcache \
        pdo_mysql \
        pdo_pgsql \
        soap \
        xml \
        zip \
    && pecl install \
        apcu \
        igbinary \
        oauth \
        uploadprogress \
    && pecl install \
        --configureoptions='enable-redis-igbinary="yes"' \
        redis \
    && docker-php-ext-enable \
        apcu \
        igbinary \
        oauth \
        redis \
        uploadprogress \
    && a2enmod \
        deflate \
        expires \
        headers \
        mime \
        rewrite \
    && a2dissite 000-default.conf \
    && apt-get remove -y $PHPIZE_DEPS \
    # Ensure make is installed.
    && apt-get install -y \
        make \
    && pecl clear-cache \
    && apt-get autoremove -y -q \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /tmp/*
